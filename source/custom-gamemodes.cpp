#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <SdkHeaders.h>
#include <custom-gamemodes/custom-gamemodes.h>

using namespace BLRE;

/// <summary>
/// FString -> std::string conversion helper taken from server-utils
/// </summary>
/// <param name="value"></param>
/// <returns></returns>
static std::string unrealStringToString(FString& value) {
    if (value.Data == NULL) {
        return std::string("");
    }
    const char* cstr = value.ToChar();
    std::string ret = std::string(cstr);
    free((void*)cstr);
    return ret;
}

static FString stringToUnrealString(const std::string& value) {
    return FString(value.c_str());
}

static FName stringToUnrealName(const std::string& value) {
    return FName(value.c_str());
}

/// <summary>
/// Broadcasts a "Server" message. Uses the PRI with player ID 9001 which should have been created beforehand
/// </summary>
/// <param name="msg"></param>
void globalAdminBroadcast(const std::string& msg) {
    AWorldInfo* curWorldInfo;
    bool foundWorldInfo = false;
    std::vector<AWorldInfo*> AWIs = UObject::GetInstancesOf<AWorldInfo>();
    for (auto& AWI : AWIs) {
        if (AWI->IsServer()) {
            curWorldInfo = AWI;
            foundWorldInfo = true;
            break;
        }
    }

    if (!foundWorldInfo)
    {
        Log->error("globalAdminBroadcast: couldn't find server's AWorldInfo!");
        return;
    }

    AFoxGame* curFoxGame = UObject::GetInstanceOf<AFoxGame>();

    std::vector<APlayerReplicationInfo*> PRIs = UObject::GetInstancesOf<APlayerReplicationInfo>();

    AFoxPRI* adminPRI = curFoxGame->FGRI->GetPRIByPlayerID(9001);

    std::vector<AFoxPC*> APCs = UObject::GetInstancesOf<AFoxPC>();

    for (auto& APC : APCs) {
        curWorldInfo->Game->BroadcastHandler->BroadcastText(adminPRI, APC, stringToUnrealString(msg), stringToUnrealName("L"));
    }

    return;
}

/// <summary>
/// Runs when a hardsuit is called by a player using the targeter
/// </summary>
/// <param name="Object"></param>
/// <param name="Stack"></param>
/// <param name="Result"></param>
/// <returns></returns>
bool HandleHardsuitSpawn(UObject* Object, FFrame& Stack, void* const Result)
{
    auto hardsuitSpawner = (AFoxWeapon_HardSuitTargeterBase*)Object;
    
    //Just for logs
    std::string playerName = unrealStringToString(hardsuitSpawner->Instigator->PlayerReplicationInfo->PlayerName);
    std::string playerID = std::to_string(hardsuitSpawner->Instigator->PlayerReplicationInfo->PlayerID);
    std::string hardsuitType = unrealStringToString(hardsuitSpawner->HardSuitRefName);
    
    Log->debug("HandleHardsuitSpawn: Player " + playerName + " with player ID " + playerID + " spawned a hardsuit of type: "+ hardsuitType);
    
    AFoxPawn_HardSuit* myHardsuit;
    bool otherHardsuitExists = false;
    bool hardsuitFound = false;

    if (hardsuitType == "FoxPawn_HardSuitLightContent") {
        // Kill the fool and its overweight jogger armor
        Log->info("HandleHardsuitSpawn: Player (" + playerName + "," + playerID + ") attempted to spawn a light hardsuit, murdering both of them for the inconvenience");
        hardsuitSpawner->Instigator->Suicide();
        std::vector<AFoxPawn_HardSuitLight*> allLightHardsuits = UObject::GetInstancesOf<AFoxPawn_HardSuitLight>();
        for (auto& lightHardsuit : allLightHardsuits) {
            lightHardsuit->Suicide();
        }
        globalAdminBroadcast(playerName + " attempted to spawn a fat jogger and was murdered for it");
        return false;
    }
    else {
    std::vector<AFoxPawn_HardSuit*> allHardsuits = UObject::GetInstancesOf<AFoxPawn_HardSuit>();

    // Bit of a mess, could use a refactor.
    // In a nutshell: get all hardsuits, check for ownership, ensure there are no other hardsuits and/or player doesn't have two hardsuits in the game
    // No early break since it's necessary to iterate over the entire vector to catch potential other hardsuits, since it doesn't seem to be predictably sorted
    for (auto& hardsuit : allHardsuits) {
        if (hardsuit->OwningPlayerId == hardsuitSpawner->Instigator->PlayerReplicationInfo->PlayerID) {
            if (!hardsuitFound) {
                Log->debug("HandleHardsuitSpawn: Found player (" + playerName + "," + playerID + ") hardsuit");
                myHardsuit = hardsuit;
                hardsuitFound = true;
            }
            else {
                // Shouldn't ever happen since we override the ID later, but who knows
                Log->info("HandleHardsuitSpawn: Player (" + playerName + "," + playerID + ") spawned another hardsuit, killing it");
                hardsuit->Suicide();
                return false;
            }
        }
        if (hardsuit->OwningPlayerId != 0 &&
            !(hardsuit->OwningPlayerId == hardsuitSpawner->Instigator->PlayerReplicationInfo->PlayerID) &&
            !(hardsuit->OwningPlayerId == 9001 && !hardsuit->IsAliveAndWell())) {
            Log->info("HandleHardsuitSpawn: Player (" + playerName + "," + playerID + ") spawned a hardsuit but there already is one in the game; it should die now");
            otherHardsuitExists = true;
        }
    }
    }

    if (otherHardsuitExists && hardsuitFound) {
        myHardsuit->Suicide();
        return false;
    }

    if (!hardsuitFound) {
        Log->warn("HandleHardsuitSpawn: Didn't find a hardsuit for player (" + playerName + "," + playerID + ") despite triggering the spawn function");
        return false;
    }

    // Could reasonably put all that as default for AFoxPawn_HardSuit derived classes, but eh
    // Set owner ID to something fantastical to make owner use the same timer as anyone else and facilitate discovery of other hardsuits later
    myHardsuit->OwningPlayerId = 9001;
    // Add a bit of challenge to claim the hardsuit
    myHardsuit->OtherHardSuitEnterTime = 1.0;

    // Buff by 5k HP
    myHardsuit->Health = 20000;
    myHardsuit->HealthMax = 20000;
    // Enable health recharge
    myHardsuit->HealthRechargeDelay = 5;
    //myHardsuit->HealthRechargePercentPerSecond = 0.010;
    myHardsuit->HealthRechargeMaxPercent = 0.75;

    myHardsuit->Stamina = 2000;
    myHardsuit->StaminaMax = 2000;
    myHardsuit->StaminaRegenRatePerSecond = 500;

    Log->info("HandleHardsuitSpawn: Hardsuit is now in the game, initiated by player (" + playerName + "," + playerID + ")");

    globalAdminBroadcast(playerName + " spawned a hardsuit!");

    return false;
}

/// <summary>
/// Runs when someone enters a hardsuit
/// </summary>
/// <param name="Object"></param>
/// <param name="Stack"></param>
/// <param name="Result"></param>
/// <returns></returns>
bool HandleHardsuitDriver(UObject* Object, FFrame& Stack, void* const Result)
{
    auto fnParams = (AFoxVehicle_execDriverEnter_Parms*)Stack.Locals;

    APawn* driver = fnParams->P;

    std::string playerName = unrealStringToString(driver->PlayerReplicationInfo->PlayerName);
    std::string playerID = std::to_string(driver->PlayerReplicationInfo->PlayerID);

    Log->info("HandleHardsuitDriver: Player (" + playerName + "," + playerID + ") took the driver seat of the hardsuit, giving score periodically");

    globalAdminBroadcast(playerName + " got the hardsuit!");

    // Gives a "payload driver" notification to player
    std::vector<AFoxPC*> AFPs = UObject::GetInstancesOf<AFoxPC>();

    AFoxPC* curFoxPC;
    bool foundFoxPC = false;

    for (auto& AFP : AFPs) {
        if (AFP->MyFoxPawn != NULL) {
            if (AFP->MyFoxPawn->PlayerReplicationInfo == driver->PlayerReplicationInfo) {
                curFoxPC = AFP;
                foundFoxPC = true;
                break;
            }
        }
    }

    if (!foundFoxPC) {
        Log->error("HandleHardsuitDriver: Couldn't find hardsuit driver's (" + playerName + "," + playerID + ") FoxPC!");
        return false;
    }

    curFoxPC->ClientReliableTeamScoreEvent(21, 0);

    // Says "captured node" globally. Wish it wasn't all structs and that I could pass whatever :)
    AFoxGame* curFoxGame = UObject::GetInstanceOf<AFoxGame>();
    AFoxPRI* curFoxPRI = curFoxGame->FGRI->GetPRIByPlayerID(driver->PlayerReplicationInfo->PlayerID);
    curFoxGame->BroadcastCombatUpdate(curFoxPRI, 8);
    
    return false;
}

/// <summary>
/// Emulates Engine.GameInfo.Login to create a server-owned PRI
/// </summary>
/// <param name="Object"></param>
/// <param name="Stack"></param>
/// <param name="Result"></param>
/// <returns></returns>
bool CreateServerPRI(UObject* Object, FFrame& Stack, void* const Result)
{
    AWorldInfo* curWorldInfo;
    bool foundAWI = false;
    std::vector<AWorldInfo*> AWIs = UObject::GetInstancesOf<AWorldInfo>();
    for (auto& AWI : AWIs) {
        if (AWI->IsServer()) {
            curWorldInfo = AWI;
            foundAWI = true;
            break;
        }
    }

    if (!foundAWI)
    {
        Log->error("CreateServerPRI: couldn't get AWorldInfo!");
        return true;
    }

    auto newPlayer = curWorldInfo->Game->Spawn(curWorldInfo->Game->PlayerReplicationInfoClass, curWorldInfo, NULL, FVector(0.0, 0.0, 0.0), FRotator(), NULL, NULL);
    
    std::vector<APlayerReplicationInfo*> PRIs = UObject::GetInstancesOf<APlayerReplicationInfo>();
    for (auto& PRI : PRIs) {
        if (PRI->Owner == curWorldInfo) {
            PRI->UniqueId.Uid.A = 9001;
            PRI->UniqueId.Uid.B = 9001;
            PRI->PlayerID = 9001;
            PRI->PlayerName = "Server";
            PRI->bAdmin = 1;
            break;
        }
    }

    // This allows admin commands.
    // It's broken as hell and boots everyone from the game when the admin leaves.
    // Leaving as reminder to investigate more later
    /*std::vector<UFoxGameSettingsCommon*> FGSs = UObject::GetInstancesOf<UFoxGameSettingsCommon>();

    for (auto& FGS : FGSs) {
        FGS->SetServerType(2);
        FGS->SetStringProperty(268435592, "2da4cb74-a7cf-4916-a65f-9598bdfc11a7");
    }*/

    return true;
}

/**
 * Initialization of the module. 
 * 
 * @remark **Do not change the name or remove the __declspec(dllexport) from this function, otherwise BLRevive will fail to load the module!**
 * 
 * @param blre pointer to BLRevive API
*/
extern "C" __declspec(dllexport) void InitializeModule(BLRevive *blre)
{
    // safe reference to blrevive api
    BLReviveAPI = blre;

    // create a logger instance for this module
    Log = blre->LogFactory->Get("capture-the-hardsuit");
    Log->info("Initializing capture-the-hardsuit");

    blre->FunctionDetour->HookPost("FoxGame.FoxWeapon_HardSuitTargeterBase.SpawnHardSuit", HandleHardsuitSpawn);

    blre->FunctionDetour->HookPre("FoxGame.FoxVehicle.DriverEnter", HandleHardsuitDriver);

    // Too early
    //blre->FunctionDetour->HookPost("FoxGame.FoxDataStore_AIPresets.Registered", HijackPRI);
    // Called too often...? It's looping despite the return true, kinda stumped there
    //blre->FunctionDetour->HookPre("Engine.WorldInfo.GetMapName", HijackPRI);
    // SOMEHOW this makes the functions not called (or at least the proxy doesn't output them in the debug log nor does it trigger the hooks)
    //blre->FunctionDetour->HookPost("Engine.WorldInfo.PreBeginPlay", HijackPRI);
    //blre->FunctionDetour->HookPost("Engine.WorldInfo.PostBeginPlay", HijackPRI);
    // Much better
    blre->FunctionDetour->HookPost("FoxGame.FoxPRI.IsValidPlayer", CreateServerPRI);
}