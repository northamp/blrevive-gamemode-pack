# BLRevive Gamemode pack

Module allowing various flavors of custom gamemodes for [Blacklight Revive](https://gitlab.com/blrevive).

## Usage

Documentation pending

## Building

Refer to the [skeleton module](https://gitlab.com/blrevive/modules/skeleton)'s more up to date and detailed instructions.

## prerequisites

In order to succesfully compile and run the module you need the following applications:

- Visual Studio 2019 (with C++/MSVC)
- patched Blacklight: Retribution installation

> Because Blacklight: Retribution itself is compiled for Win32, the modules target that platform too and only that one. It may be possible to compile the modules for another platform but there is no point in doing so.

1. change `GIT_TAG main` and `set(PROXY_VERSION "0.0.1" ...` in cmake/packages.cmake to the version of Proxy you are targetting
2. start VS developer shell inside project
3. run `cmake -A Win32 -B build/` to generate build files for VS 2019 / Win32
4. configure debugging feature with cmake options (see below)

### cmake options

| option            | description                                         | default                                                                    |
| ----------------- | --------------------------------------------------- | -------------------------------------------------------------------------- |
| `BLR_INSTALL_DIR` | absolute path to Blacklight install directory       | `C:\\Program Files (x86)\\Steam\\steamapps\\common\\blacklightretribution` |
| `BLR_EXECUTABLE`  | filename of BLR applicaiton                         | `BLR.exe`                                                                  |
| `BLR_SERVER_CLI`  | command line params passed to server when debugging | `server HeloDeck`                                                          |
| `BLR_CLIENT_CLI`  | command line params passed to client when debugging | `127.0.0.1?Name=superewald`                                                |
